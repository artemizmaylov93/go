package main

import "fmt"

func main() {
	x := []int{
		48, 96, 86, 68,
		57, 82, 63, 70,
		37, 34, 2, 27,
		19, 97, 9, 17,
	}
	imin := 0
	for i := 0; i < len(x); i++ {
		if x[imin] > x[i] {
			imin = i
		}
	}
	fmt.Println(x[imin])
}
