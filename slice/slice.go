package main

import "fmt"

func main() {
	slice1 := []int{1, 2, 3}
	slice2 := append(slice1, 4, 5)
	fmt.Println(slice1, slice2)
}

/*func main() {
	slice1 := []int{1, 2, 3, 4}
	slice2 := make([]int, 2)
	copy(slice2, slice1)
	fmt.Println(slice1, slice2)
}
*/

/*func main() {
	m := 1
	x := [1]int{m}
	fmt.Println(x)
}
*/
